use jom::ClassFile;

#[test]
fn parse() {
    let bytes = include_bytes!("Test1.class");

    ClassFile::parse(bytes).unwrap();
}
