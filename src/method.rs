use nom::{multi::length_count, number::complete::be_u16, sequence::tuple, IResult};

use crate::{
    access_flags::{method_access_flags, MethodAccessFlags},
    attribute::{attribute, Attribute},
    error::JomError,
};

#[derive(Debug)]
pub struct MethodInfo<'a> {
    access_flags: MethodAccessFlags,
    name: u16,
    descriptor: u16,
    attributes: Vec<Attribute<'a>>,
}

impl<'a> MethodInfo<'a> {
    /// Get the method's access flags.
    pub fn access_flags(&self) -> MethodAccessFlags {
        self.access_flags
    }

    /// Get the method name.
    pub fn name(&self) -> u16 {
        self.name
    }

    /// Get the method descriptor.
    pub fn descriptor(&self) -> u16 {
        self.descriptor
    }

    /// Get the method's attributes.
    pub fn attributes(&self) -> &[Attribute] {
        &self.attributes
    }
}

pub fn method(data: &[u8]) -> IResult<&[u8], MethodInfo, JomError> {
    let (data, (access_flags, name, descriptor, attributes)) = tuple((
        method_access_flags,
        be_u16,
        be_u16,
        length_count(be_u16, attribute),
    ))(data)?;

    Ok((
        data,
        MethodInfo {
            access_flags,
            name,
            descriptor,
            attributes,
        },
    ))
}
