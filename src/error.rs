use std::str::Utf8Error;

use nom::error::{ErrorKind, ParseError};

#[derive(Debug, PartialEq)]
pub enum JomError<'a> {
    Nom(&'a [u8], ErrorKind),
    Utf8Error(Utf8Error),
    InvalidTag(u8),
    UnexpectedTag(&'static str, &'static str),
    InvalidMethodHandle(&'static str),
    InvalidMethodHandleKind(u8),
    ConstantPoolIndexOutOfBounds,
}

impl<'a> From<(&'a [u8], ErrorKind)> for JomError<'a> {
    fn from((i, ek): (&'a [u8], ErrorKind)) -> Self {
        Self::Nom(i, ek)
    }
}

impl<'a> ParseError<&'a [u8]> for JomError<'a> {
    fn from_error_kind(input: &'a [u8], kind: ErrorKind) -> Self {
        Self::Nom(input, kind)
    }

    fn append(_: &[u8], _: ErrorKind, other: Self) -> Self {
        other
    }
}
