use nom::{multi::length_count, number::complete::be_u16, sequence::tuple, IResult};

use crate::{
    access_flags::{field_access_flags, FieldAccessFlags},
    attribute::{attribute, Attribute},
    error::JomError,
};

#[derive(Debug)]
pub struct FieldInfo<'a> {
    access_flags: FieldAccessFlags,
    name: u16,
    descriptor: u16,
    attributes: Vec<Attribute<'a>>,
}

impl<'a> FieldInfo<'a> {
    /// Get the field's access flags.
    pub fn access_flags(&self) -> FieldAccessFlags {
        self.access_flags
    }

    /// Get the field name.
    pub fn name(&self) -> u16 {
        self.name
    }

    /// Get the field descriptor.
    pub fn descriptor(&self) -> u16 {
        self.descriptor
    }

    /// Get the field's attributes.
    pub fn attributes(&self) -> &[Attribute] {
        &self.attributes
    }
}

pub fn field(data: &[u8]) -> IResult<&[u8], FieldInfo, JomError> {
    let (data, (access_flags, name, descriptor, attributes)) = tuple((
        field_access_flags,
        be_u16,
        be_u16,
        length_count(be_u16, attribute),
    ))(data)?;

    Ok((
        data,
        FieldInfo {
            access_flags,
            name,
            descriptor,
            attributes,
        },
    ))
}
