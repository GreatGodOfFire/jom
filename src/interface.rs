use crate::{constant_pool::ConstantInfo, error::JomError};

pub fn get_interface_names<'a>(
    constant_pool: &[ConstantInfo<'a>],
    interface_indexes: Vec<u16>,
) -> Result<Vec<&'a str>, JomError<'static>> {
    let mut interfaces = Vec::with_capacity(interface_indexes.len());

    for interface in interface_indexes {
        let info = constant_pool
            .get(interface as usize)
            .ok_or(JomError::ConstantPoolIndexOutOfBounds)?;
        let name = info
            .as_class()
            .ok_or(JomError::UnexpectedTag("Class", info.name()))?;

        interfaces.push(name);
    }

    Ok(interfaces)
}
