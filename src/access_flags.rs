use bitflags::bitflags;
use nom::{number::complete::be_u16, IResult};

use crate::error::JomError;

bitflags! {
    pub struct FieldAccessFlags: u16 {
        const PUBLIC = 0x0001;
        const PRIVATE = 0x0002;
        const PROTECTED = 0x0004;
        const STATIC = 0x0008;
        const FINAL = 0x0010;
        const VOLATILE = 0x0040;
        const TRANSIENT = 0x0080;
        const SYNTHETIC = 0x1000;
        const ENUM = 0x4000;
    }

    pub struct MethodAccessFlags: u16 {
        const PUBLIC = 0x0001;
        const PRIVATE = 0x0002;
        const PROTECTED = 0x0004;
        const STATIC = 0x0008;
        const FINAL = 0x0010;
        const SYNCHRONIZED = 0x0020;
        const BRIDGE = 0x0040;
        const VARARGS = 0x0080;
        const NATIVE = 0x0100;
        const ABSTRACT = 0x0400;
        const STRICT = 0x0800;
        const SYNTHETIC = 0x1000;
    }

    pub struct ClassAccessFlags: u16 {
        const PUBLIC = 0x0001;
        const FINAL = 0x0010;
        const SUPER = 0x0020;
        const INTERFACE = 0x0200;
        const ABSTRACT = 0x0400;
        const SYNTHETIC = 0x1000;
        const ANNOTATION = 0x2000;
        const ENUM = 0x4000;
        const MODULE = 0x8000;
    }
}

pub fn field_access_flags(data: &[u8]) -> IResult<&[u8], FieldAccessFlags, JomError> {
    be_u16(data).map(|(d, f)| (d, FieldAccessFlags::from_bits_truncate(f)))
}

pub fn method_access_flags(data: &[u8]) -> IResult<&[u8], MethodAccessFlags, JomError> {
    be_u16(data).map(|(d, f)| (d, MethodAccessFlags::from_bits_truncate(f)))
}

pub fn class_access_flags(data: &[u8]) -> IResult<&[u8], ClassAccessFlags, JomError> {
    be_u16(data).map(|(d, f)| (d, ClassAccessFlags::from_bits_truncate(f)))
}
