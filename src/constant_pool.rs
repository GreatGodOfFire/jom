use std::str::from_utf8;

use nom::{
    multi::length_data,
    number::complete::{be_f32, be_f64, be_i32, be_i64, be_u16, be_u8},
    sequence::tuple,
    IResult,
};

use crate::error::JomError;

#[derive(Debug, Clone)]
pub enum ConstantInfo<'a> {
    Length(u16),
    Utf8(&'a str),
    Integer(i32),
    Float(f32),
    Long(i64),
    Double(f64),
    Class(&'a str),
    String(&'a str),
    Fieldref {
        class: &'a str,
        name: &'a str,
        descriptor: &'a str,
    },
    Methodref {
        class: &'a str,
        name: &'a str,
        descriptor: &'a str,
    },
    InterfaceMethodref {
        class: &'a str,
        name: &'a str,
        descriptor: &'a str,
    },
    NameAndType {
        name: &'a str,
        descriptor: &'a str,
    },
    MethodHandle {
        kind: MethodHandleKind,
        class: &'a str,
        name: &'a str,
        descriptor: &'a str,
    },
    MethodType(&'a str),
    Dynamic {
        bootstrap_method_idx: u16,
        name: &'a str,
        descriptor: &'a str,
    },
    InvokeDynamic {
        bootstrap_method_idx: u16,
        name: &'a str,
        descriptor: &'a str,
    },
    Module(&'a str),
    Package(&'a str),
    Unusable,
}

impl<'a> ConstantInfo<'a> {
    pub fn as_length(&self) -> Option<u16> {
        if let Self::Length(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn as_utf8(&self) -> Option<&'a str> {
        if let Self::Utf8(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_integer(&self) -> Option<i32> {
        if let Self::Integer(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn as_float(&self) -> Option<f32> {
        if let Self::Float(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn as_long(&self) -> Option<i64> {
        if let Self::Long(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn as_double(&self) -> Option<f64> {
        if let Self::Double(v) = self {
            Some(*v)
        } else {
            None
        }
    }

    pub fn as_class(&self) -> Option<&'a str> {
        if let Self::Class(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_string(&self) -> Option<&'a str> {
        if let Self::String(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_fieldref(&self) -> Option<(&'a str, &'a str, &'a str)> {
        if let Self::Fieldref {
            class,
            name,
            descriptor,
        } = self
        {
            Some((class, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_methodref(&self) -> Option<(&'a str, &'a str, &'a str)> {
        if let Self::Methodref {
            class,
            name,
            descriptor,
        } = self
        {
            Some((class, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_interface_methodref(&self) -> Option<(&'a str, &'a str, &'a str)> {
        if let Self::InterfaceMethodref {
            class,
            name,
            descriptor,
        } = self
        {
            Some((class, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_name_and_type(&self) -> Option<(&'a str, &'a str)> {
        if let Self::NameAndType { name, descriptor } = self {
            Some((name, descriptor))
        } else {
            None
        }
    }

    pub fn as_method_handle(&self) -> Option<(MethodHandleKind, &'a str, &'a str, &'a str)> {
        if let Self::MethodHandle {
            kind,
            class,
            name,
            descriptor,
        } = self
        {
            Some((*kind, class, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_method_type(&self) -> Option<&'a str> {
        if let Self::MethodType(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_dynamic(&self) -> Option<(u16, &'a str, &'a str)> {
        if let Self::Dynamic {
            bootstrap_method_idx,
            name,
            descriptor,
        } = self
        {
            Some((*bootstrap_method_idx, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_invoke_dynamic(&self) -> Option<(u16, &'a str, &'a str)> {
        if let Self::InvokeDynamic {
            bootstrap_method_idx,
            name,
            descriptor,
        } = self
        {
            Some((*bootstrap_method_idx, name, descriptor))
        } else {
            None
        }
    }

    pub fn as_module(&self) -> Option<&'a str> {
        if let Self::Module(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_package(&self) -> Option<&'a str> {
        if let Self::Package(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the constant info is [`Unusable`].
    ///
    /// [`Unusable`]: ConstantInfo::Unusable
    pub fn is_unusable(&self) -> bool {
        matches!(self, Self::Unusable)
    }

    pub fn name(&self) -> &'static str {
        match self {
            ConstantInfo::Length(_) => "Length",
            ConstantInfo::Utf8(_) => "Utf8",
            ConstantInfo::Integer(_) => "Integer",
            ConstantInfo::Float(_) => "Float",
            ConstantInfo::Long(_) => "Long",
            ConstantInfo::Double(_) => "Double",
            ConstantInfo::Class(_) => "Class",
            ConstantInfo::String(_) => "String",
            ConstantInfo::Fieldref { .. } => "Fieldref",
            ConstantInfo::Methodref { .. } => "Methodref",
            ConstantInfo::InterfaceMethodref { .. } => "InterfaceMethodref",
            ConstantInfo::NameAndType { .. } => "NameAndType",
            ConstantInfo::MethodHandle { .. } => "MethodHandle",
            ConstantInfo::MethodType(_) => "MethodType",
            ConstantInfo::Dynamic { .. } => "Dynamic",
            ConstantInfo::InvokeDynamic { .. } => "InvokeDynamic",
            ConstantInfo::Module(_) => "Module",
            ConstantInfo::Package(_) => "Package",
            ConstantInfo::Unusable => "Unusable",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum MethodHandleKind {
    GetField,
    GetStatic,
    PutField,
    PutStatic,
    InvokeVirtual,
    InvokeStatic,
    InvokeSpecial,
    NewInvokeSpecial,
    InvokeInterface,
}

impl TryFrom<u8> for MethodHandleKind {
    type Error = JomError<'static>;

    fn try_from(i: u8) -> Result<Self, Self::Error> {
        match i {
            1 => Ok(Self::GetField),
            2 => Ok(Self::GetStatic),
            3 => Ok(Self::PutField),
            4 => Ok(Self::PutStatic),
            5 => Ok(Self::InvokeVirtual),
            6 => Ok(Self::InvokeStatic),
            7 => Ok(Self::InvokeSpecial),
            8 => Ok(Self::NewInvokeSpecial),
            9 => Ok(Self::InvokeInterface),
            _ => Err(JomError::InvalidMethodHandleKind(i)),
        }
    }
}

enum RawConstantInfo<'a> {
    Length(u16),
    Utf8(&'a str),
    Integer(i32),
    Float(f32),
    Long(i64),
    Double(f64),
    Class(u16),
    String(u16),
    Fieldref(u16, u16),
    Methodref(u16, u16),
    InterfaceMethodref(u16, u16),
    NameAndType(u16, u16),
    MethodHandle(u8, u16),
    MethodType(u16),
    Dynamic(u16, u16),
    InvokeDynamic(u16, u16),
    Module(u16),
    Package(u16),
    Unusable,
}

impl<'a> RawConstantInfo<'a> {
    fn name(&self) -> &'static str {
        match self {
            RawConstantInfo::Length(_) => "Length",
            RawConstantInfo::Utf8(_) => "Utf8",
            RawConstantInfo::Integer(_) => "Integer",
            RawConstantInfo::Float(_) => "Float",
            RawConstantInfo::Long(_) => "Long",
            RawConstantInfo::Double(_) => "Double",
            RawConstantInfo::Class(_) => "Class",
            RawConstantInfo::String(_) => "String",
            RawConstantInfo::Fieldref(_, _) => "Fieldref",
            RawConstantInfo::Methodref(_, _) => "Methodref",
            RawConstantInfo::InterfaceMethodref(_, _) => "InterfaceMethodref",
            RawConstantInfo::NameAndType(_, _) => "NameAndType",
            RawConstantInfo::MethodHandle(_, _) => "MethodHandle",
            RawConstantInfo::MethodType(_) => "MethodType",
            RawConstantInfo::Dynamic(_, _) => "Dynamic",
            RawConstantInfo::InvokeDynamic(_, _) => "InvokeDynamic",
            RawConstantInfo::Module(_) => "Module",
            RawConstantInfo::Package(_) => "Package",
            RawConstantInfo::Unusable => "Unusable",
        }
    }
}

pub fn constant_pool(data: &[u8]) -> IResult<&[u8], Vec<ConstantInfo>, JomError> {
    let (mut data, len) = be_u16(data)?;

    let mut raw_constant_pool = Vec::with_capacity(len as usize);
    raw_constant_pool.push(RawConstantInfo::Length(len));

    let mut i = 1;

    while i < len {
        let (d, entry) = constant_entry(data)?;
        if let RawConstantInfo::Long(_) | RawConstantInfo::Double(_) = entry {
            raw_constant_pool.push(entry);
            raw_constant_pool.push(RawConstantInfo::Unusable);

            i += 1;
        } else {
            raw_constant_pool.push(entry);
        }

        data = d;

        i += 1;
    }

    let mut constant_pool = vec![None; (len - 1) as usize];

    for idx in 0..len - 1 {
        resolve_entry(&mut constant_pool, &raw_constant_pool, idx).map_err(nom::Err::Error)?;
    }

    Ok((
        data,
        constant_pool.into_iter().map(Option::unwrap).collect(),
    ))
}

fn constant_entry(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    let (data, tag) = be_u8(data)?;
    let fun = match tag {
        1 => utf8,
        3 => integer,
        4 => float,
        5 => long,
        6 => double,
        7 => class,
        8 => string,
        9 => fieldref,
        10 => methodref,
        11 => interface_methodref,
        12 => name_and_type,
        15 => method_handle,
        16 => method_type,
        17 => dynamic,
        18 => invoke_dynamic,
        19 => module,
        20 => package,
        i => return Err(nom::Err::Error(JomError::InvalidTag(i))),
    };

    fun(data)
}

fn resolve_entry<'a>(
    constant_pool: &mut Vec<Option<ConstantInfo<'a>>>,
    raw_constant_pool: &[RawConstantInfo<'a>],
    idx: u16,
) -> Result<(), JomError<'a>> {
    constant_pool[idx as usize] = match raw_constant_pool[idx as usize] {
        RawConstantInfo::Length(l) => Some(ConstantInfo::Length(l)),
        RawConstantInfo::Utf8(s) => Some(ConstantInfo::Utf8(s)),
        RawConstantInfo::Integer(i) => Some(ConstantInfo::Integer(i)),
        RawConstantInfo::Float(f) => Some(ConstantInfo::Float(f)),
        RawConstantInfo::Long(l) => Some(ConstantInfo::Long(l)),
        RawConstantInfo::Double(d) => Some(ConstantInfo::Double(d)),
        RawConstantInfo::Class(idx) => Some(ConstantInfo::Class(get_utf8(raw_constant_pool, idx)?)),
        RawConstantInfo::String(idx) => {
            Some(ConstantInfo::String(get_utf8(raw_constant_pool, idx)?))
        }
        RawConstantInfo::Fieldref(class, name_and_type) => {
            let class = get_class(raw_constant_pool, class)?;
            let (name, descriptor) = get_name_and_type(raw_constant_pool, name_and_type)?;

            Some(ConstantInfo::Fieldref {
                class,
                name,
                descriptor,
            })
        }
        RawConstantInfo::Methodref(class, name_and_type) => {
            let class = get_class(raw_constant_pool, class)?;
            let (name, descriptor) = get_name_and_type(raw_constant_pool, name_and_type)?;

            Some(ConstantInfo::Methodref {
                class,
                name,
                descriptor,
            })
        }
        RawConstantInfo::InterfaceMethodref(class, name_and_type) => {
            let class = get_class(raw_constant_pool, class)?;
            let (name, descriptor) = get_name_and_type(raw_constant_pool, name_and_type)?;

            Some(ConstantInfo::InterfaceMethodref {
                class,
                name,
                descriptor,
            })
        }
        RawConstantInfo::NameAndType(name, descriptor) => Some(ConstantInfo::NameAndType {
            name: get_utf8(raw_constant_pool, name)?,
            descriptor: get_utf8(raw_constant_pool, descriptor)?,
        }),
        RawConstantInfo::MethodHandle(kind, reference) => {
            let kind = MethodHandleKind::try_from(kind)?;
            let info = &raw_constant_pool[reference as usize];

            let (class, name, descriptor) = match kind {
                MethodHandleKind::GetField
                | MethodHandleKind::GetStatic
                | MethodHandleKind::PutField
                | MethodHandleKind::PutStatic => {
                    if let RawConstantInfo::Fieldref(class, name_and_ty) = info {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag("Fieldref", info.name()));
                    }
                }
                MethodHandleKind::InvokeVirtual => {
                    if let RawConstantInfo::Methodref(class, name_and_ty) = info {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        if name == "<init>" || name == "<clinit>" {
                            return Err(JomError::InvalidMethodHandle(
                                "The name of a InvokeVirtual MethodHandle must not be <init> or <clinit>.",
                            ));
                        }

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag("Methodref", info.name()));
                    }
                }
                MethodHandleKind::InvokeStatic => {
                    if let RawConstantInfo::Fieldref(class, name_and_ty)
                    | RawConstantInfo::Methodref(class, name_and_ty) = info
                    {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        if name == "<init>" || name == "<clinit>" {
                            return Err(JomError::InvalidMethodHandle(
                                "The name of a InvokeStatic MethodHandle must not be <init> or <clinit>.",
                            ));
                        }

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag(
                            "Fieldref or Methodref",
                            info.name(),
                        ));
                    }
                }
                MethodHandleKind::InvokeSpecial => {
                    if let RawConstantInfo::Fieldref(class, name_and_ty)
                    | RawConstantInfo::Methodref(class, name_and_ty) = info
                    {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        if name == "<init>" || name == "<clinit>" {
                            return Err(JomError::InvalidMethodHandle(
                                "The name of a InvokeSpecial MethodHandle must not be <init> or <clinit>.",
                            ));
                        }

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag(
                            "Fieldref or Methodref",
                            info.name(),
                        ));
                    }
                }
                MethodHandleKind::NewInvokeSpecial => {
                    if let RawConstantInfo::Methodref(class, name_and_ty) = info {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        if name != "<init>" {
                            return Err(JomError::InvalidMethodHandle(
                                "The name of a NewInvokeSpecial MethodHandle must be <init>.",
                            ));
                        }

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag("Methodref", info.name()));
                    }
                }
                MethodHandleKind::InvokeInterface => {
                    if let RawConstantInfo::InterfaceMethodref(class, name_and_ty) = info {
                        let class = get_class(raw_constant_pool, *class)?;
                        let (name, descriptor) =
                            get_name_and_type(raw_constant_pool, *name_and_ty)?;

                        if name == "<init>" || name == "<clinit>" {
                            return Err(JomError::InvalidMethodHandle(
                                "The name of a InvokeInterface MethodHandle must not be <init> or <clinit>.",
                            ));
                        }

                        (class, name, descriptor)
                    } else {
                        return Err(JomError::UnexpectedTag("InterfaceMethodref", info.name()));
                    }
                }
            };

            Some(ConstantInfo::MethodHandle {
                kind,
                class,
                name,
                descriptor,
            })
        }
        RawConstantInfo::MethodType(idx) => {
            Some(ConstantInfo::MethodType(get_utf8(raw_constant_pool, idx)?))
        }
        RawConstantInfo::Dynamic(bootstrap_method_idx, name_and_ty) => {
            let (name, descriptor) = get_name_and_type(raw_constant_pool, name_and_ty)?;

            Some(ConstantInfo::Dynamic {
                bootstrap_method_idx,
                name,
                descriptor,
            })
        }
        RawConstantInfo::InvokeDynamic(bootstrap_method_idx, name_and_ty) => {
            let (name, descriptor) = get_name_and_type(raw_constant_pool, name_and_ty)?;

            Some(ConstantInfo::InvokeDynamic {
                bootstrap_method_idx,
                name,
                descriptor,
            })
        }
        RawConstantInfo::Module(idx) => {
            Some(ConstantInfo::Module(get_utf8(raw_constant_pool, idx)?))
        }
        RawConstantInfo::Package(idx) => {
            Some(ConstantInfo::Package(get_utf8(raw_constant_pool, idx)?))
        }
        RawConstantInfo::Unusable => Some(ConstantInfo::Unusable),
    };

    Ok(())
}

fn get_utf8<'a>(
    raw_constant_pool: &[RawConstantInfo<'a>],
    idx: u16,
) -> Result<&'a str, JomError<'a>> {
    let info = &raw_constant_pool[idx as usize];

    if let RawConstantInfo::Utf8(s) = info {
        Ok(s)
    } else {
        Err(JomError::UnexpectedTag("Utf8", info.name()))
    }
}

fn get_class<'a>(
    raw_constant_pool: &[RawConstantInfo<'a>],
    idx: u16,
) -> Result<&'a str, JomError<'a>> {
    let info = &raw_constant_pool[idx as usize];

    if let RawConstantInfo::Class(idx) = info {
        Ok(get_utf8(raw_constant_pool, *idx)?)
    } else {
        Err(JomError::UnexpectedTag("Class", info.name()))
    }
}

fn get_name_and_type<'a>(
    raw_constant_pool: &[RawConstantInfo<'a>],
    idx: u16,
) -> Result<(&'a str, &'a str), JomError<'a>> {
    let info = &raw_constant_pool[idx as usize];

    if let RawConstantInfo::NameAndType(name, ty) = info {
        Ok((
            get_utf8(raw_constant_pool, *name)?,
            get_utf8(raw_constant_pool, *ty)?,
        ))
    } else {
        Err(JomError::UnexpectedTag("NameAndType", info.name()))
    }
}

fn utf8(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    let (data, bytes) = length_data(be_u16)(data)?;
    from_utf8(bytes)
        .map_err(|e| nom::Err::Error(JomError::Utf8Error(e)))
        .map(|s| (data, RawConstantInfo::Utf8(s)))
}

fn integer(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_i32(data).map(|(d, i)| (d, RawConstantInfo::Integer(i)))
}

fn float(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_f32(data).map(|(d, f)| (d, RawConstantInfo::Float(f)))
}

fn long(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_i64(data).map(|(d, l)| (d, RawConstantInfo::Long(l)))
}

fn double(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_f64(data).map(|(d, f)| (d, RawConstantInfo::Double(f)))
}

fn class(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_u16(data).map(|(d, i)| (d, RawConstantInfo::Class(i)))
}

fn string(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_u16(data).map(|(d, i)| (d, RawConstantInfo::String(i)))
}

fn fieldref(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::Fieldref(i, j)))
}

fn methodref(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::Methodref(i, j)))
}

fn interface_methodref(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::InterfaceMethodref(i, j)))
}

fn name_and_type(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::NameAndType(i, j)))
}

fn method_handle(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u8, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::MethodHandle(i, j)))
}

fn method_type(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_u16(data).map(|(d, i)| (d, RawConstantInfo::MethodType(i)))
}

fn dynamic(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::Dynamic(i, j)))
}

fn invoke_dynamic(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    tuple((be_u16, be_u16))(data).map(|(d, (i, j))| (d, RawConstantInfo::InvokeDynamic(i, j)))
}

fn module(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_u16(data).map(|(d, i)| (d, RawConstantInfo::Module(i)))
}

fn package(data: &[u8]) -> IResult<&[u8], RawConstantInfo, JomError> {
    be_u16(data).map(|(d, i)| (d, RawConstantInfo::Package(i)))
}
