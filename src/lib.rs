use access_flags::{class_access_flags, ClassAccessFlags};
use attribute::{attribute, Attribute};
use constant_pool::{constant_pool, ConstantInfo};
use error::JomError;
use field::{field, FieldInfo};
use interface::get_interface_names;
use method::{method, MethodInfo};
use nom::{
    bytes::complete::tag, combinator::eof, multi::length_count, number::complete::be_u16,
    sequence::tuple, Finish,
};

pub mod access_flags;
pub mod attribute;
pub mod constant_pool;
pub mod error;
pub mod field;
pub mod interface;
pub mod method;

#[derive(Debug)]
pub struct ClassFile<'a> {
    minor: u16,
    major: u16,
    constant_pool: Vec<ConstantInfo<'a>>,
    access_flags: ClassAccessFlags,
    this_class: &'a str,
    super_class: &'a str,
    interfaces: Vec<&'a str>,
    fields: Vec<FieldInfo<'a>>,
    methods: Vec<MethodInfo<'a>>,
    attributes: Vec<Attribute<'a>>,
}

impl<'a> ClassFile<'a> {
    pub fn parse(data: &'a [u8]) -> Result<Self, JomError> {
        let (
            _,
            (
                _,
                minor,
                major,
                constant_pool,
                access_flags,
                this_class,
                super_class,
                interfaces,
                fields,
                methods,
                attributes,
                _,
            ),
        ) = Finish::<_, _, JomError>::finish(tuple((
            tag([0xCA, 0xFE, 0xBA, 0xBE]),
            be_u16,
            be_u16,
            constant_pool,
            class_access_flags,
            be_u16,
            be_u16,
            length_count(be_u16, be_u16),
            length_count(be_u16, field),
            length_count(be_u16, method),
            length_count(be_u16, attribute),
            eof,
        ))(data))?;

        let interfaces = get_interface_names(&constant_pool, interfaces)?;

        let this_class_info = &constant_pool[this_class as usize];
        let this_class = this_class_info
            .as_class()
            .ok_or(JomError::UnexpectedTag("Class", this_class_info.name()))?;
        let super_class_info = &constant_pool[super_class as usize];
        let super_class = this_class_info
            .as_class()
            .ok_or(JomError::UnexpectedTag("Class", super_class_info.name()))?;

        Ok(Self {
            minor,
            major,
            constant_pool,
            access_flags,
            this_class,
            super_class,
            interfaces,
            fields,
            methods,
            attributes,
        })
    }

    /// Get the minor version.
    pub fn minor(&self) -> u16 {
        self.minor
    }

    /// Get the major version.
    pub fn major(&self) -> u16 {
        self.major
    }

    /// Get a reference to the constant pool.
    pub fn constant_pool(&self) -> &[ConstantInfo] {
        self.constant_pool.as_ref()
    }

    /// Get the class access flags.
    pub fn access_flags(&self) -> ClassAccessFlags {
        self.access_flags
    }

    /// Get the name of the class.
    pub fn this_class(&self) -> &'a str {
        self.this_class
    }

    /// Get the super class name of the class.
    pub fn super_class(&self) -> &'a str {
        self.super_class
    }

    /// Get the interfaces of the class.
    pub fn interfaces(&self) -> &[&'a str] {
        self.interfaces.as_ref()
    }

    /// Get the fields of the class.
    pub fn fields(&self) -> &[FieldInfo] {
        &self.fields
    }

    /// Get the methods of the class.
    pub fn methods(&self) -> &[MethodInfo] {
        &self.methods
    }

    /// Get the attributes of the class.
    pub fn attributes(&self) -> &[Attribute] {
        self.attributes.as_ref()
    }
}
