use nom::{
    multi::length_data,
    number::complete::{be_u16, be_u32},
    sequence::tuple,
    IResult,
};

use crate::error::JomError;

#[derive(Debug)]
pub struct Attribute<'a> {
    name: u16,
    info: &'a [u8],
}

impl<'a> Attribute<'a> {
    /// Get the attribute's name.
    pub fn name(&self) -> u16 {
        self.name
    }

    /// Get the attribute's info.
    pub fn info(&self) -> &[u8] {
        self.info
    }
}

pub fn attribute(data: &[u8]) -> IResult<&[u8], Attribute, JomError> {
    let (data, (name, info)) = tuple((be_u16, length_data(be_u32)))(data)?;

    Ok((data, Attribute { name, info }))
}
